import { IsEmail, IsNotEmpty, IsOptional, IsString, Length } from 'class-validator';
import { Role } from 'src/common/enums/rol.enum';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsString()
  name: string;

  @Column({ unique: true, nullable: false })
  @IsEmail()
  email: string;

  @Column({ nullable: false })
  @Length(8)
  password: string;

  @Column({ type: 'enum', enum: Role, default: Role.USER })
  role: string;

  @Column({ nullable: true })
  @IsOptional()
  resetToken: string;

  @Column({ type: 'timestamp', nullable: true })
  @IsOptional()
  resetTokenExpiration?: Date;

  @Column({ nullable: true })
  @IsOptional()
  @IsString()
  phoneNumber?: string; 
}
