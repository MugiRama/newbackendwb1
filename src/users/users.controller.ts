import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Request, HttpException, HttpStatus, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Role } from 'src/common/enums/rol.enum';
import { Auth } from 'src/auth/decorator/auth.decorator';


@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Auth(Role.USER)
  @Get('profile')
  async getProfile(@Request() req: any) {
    const userEmail = req.user.email; 
    return await this.usersService.findOneByEmailPartial(userEmail);
  }

  @Auth(Role.USER)
  @Patch('profile')
  async updateProfile(@Request() req: any, @Body() updateUserDto: UpdateUserDto) {
    const userEmail = req.user.email; 
    return await this.usersService.updateByEmail(userEmail, updateUserDto);
  }


  @Get('validate-token')
  validateToken(@Query('token') token: string) {
    return this.usersService.validateResetToken(token);
  }

  @Get()
  async findAll() {
    return this.usersService.findAll();
  }
}
