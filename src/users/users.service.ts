import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository, MoreThanOrEqual } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  findOneByResetToken(token: string) {
    throw new Error('Method not implemented.');
  }
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    return this.userRepository.save(createUserDto);
  }

  async findOneByEmail(email: string): Promise<User | null> {
    return this.userRepository.findOneBy({ email });
  }

  async findAll() {
    return this.userRepository.find();
  }

  async findOneByEmailPartial(email: string): Promise<Partial<User> | null> {
    const user = await this.userRepository.findOne({
      where: { email },
      select: ['name', 'email', 'role', 'phoneNumber'], 
    });
  
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
  
    return user;
  }
  
  async updateByEmail(email: string, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.userRepository.findOne({ where: { email } });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    Object.assign(user, updateUserDto);
    return this.userRepository.save(user);
  }
  

  async findOneByEmailWithPassword(email: string): Promise<User | null> {
    return this.userRepository.findOne({
      where: { email },
      select: ['id', 'name', 'email', 'password', 'role'],
    });
  }

  async findOneBy(filters: Record<string, any>): Promise<User | null> {
    return this.userRepository.findOneBy(filters);
  }

  async save(user: User): Promise<User> {
    return this.userRepository.save(user);
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    Object.assign(user, updateUserDto);
    return this.userRepository.save(user);
  }

  async validateResetToken(token: string): Promise<User> {
    const user = await this.findOneBy({
      resetToken: token,
      resetTokenExpiration: MoreThanOrEqual(new Date())
    });
    if (!user) {
      throw new HttpException('Invalid or expired reset token', HttpStatus.UNAUTHORIZED);
    }
    return user;
  }
}
