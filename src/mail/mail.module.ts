import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => {
        console.log('MAIL_HOST:', config.get('MAIL_HOST'));
        console.log('MAIL_PORT:', config.get('MAIL_PORT'));
        console.log('MAIL_USER:', config.get('MAIL_USER'));
        console.log('MAIL_PASSWORD:', config.get('MAIL_PASSWORD'));
        return {
          transport: {
            host: config.get('MAIL_HOST'),
            port: config.get('MAIL_PORT'),
            secure: false, 
            auth: {
              user: config.get('MAIL_USER'),
              pass: config.get('MAIL_PASSWORD'),
            },
          },
          defaults: {
            from: '"No Reply" <no-reply@example.com>',
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  exports: [MailerModule],
})
export class MailModule {}
