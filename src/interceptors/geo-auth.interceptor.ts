import { CallHandler, ExecutionContext, Injectable, NestInterceptor, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getDistance } from 'geolib';

@Injectable()
export class GeoAuthInterceptor implements NestInterceptor {
    
    private baseLocation = { latitude: -29.87148, longitude: -71.3451369 };
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const request = context.switchToHttp().getRequest();
        const userLocation = {
            latitude: this.getNumber(request.body.latitude),
            longitude: this.getNumber(request.body.longitude)
        };

        console.log('User Location:', userLocation);
        console.log('Base Location:', this.baseLocation);

        if (!this.isLocationValid(userLocation, this.baseLocation, 15000)) { 
            console.error('Ubicación no válida para autenticación:', userLocation);
            throw new UnauthorizedException('Ubicación no válida para autenticación');
        }

        return next.handle().pipe(
            map(data => data)
        );
    }

    isLocationValid(userLocation, baseLocation, maxDistance): boolean {
        try {
            const distance = getDistance(userLocation, baseLocation);
            console.log('Calculated distance:', distance);
            return distance <= maxDistance;
        } catch (error) {
            console.error('Error calculating distance:', error);
            return false;
        }
    }

    private getNumber(value: any): number | null {
        const parsed = parseFloat(value);
        return isNaN(parsed) ? null : parsed;
    }
}
