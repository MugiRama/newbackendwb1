import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcryptjs from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { MailerService } from '@nestjs-modules/mailer';
import { MoreThanOrEqual } from 'typeorm';
import { jwtConstants } from './constant/jwt.constant';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { ResetPasswordRequestDto } from './dto/reset-request-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private mailerService: MailerService
  ) { }

  async register(registerDto: RegisterDto): Promise<void> {
    const existingUser = await this.usersService.findOneByEmail(registerDto.email);
    if (existingUser) {
      throw new BadRequestException('User already exists');
    }
    const hashedPassword = await bcryptjs.hash(registerDto.password, 10);
    const user = {
      ...registerDto,
      password: hashedPassword
    };
    await this.usersService.create(user);
  }


  async login(loginDto: LoginDto): Promise<{ token: string; email: string; role: string }> {
    const user = await this.usersService.findOneByEmailWithPassword(loginDto.email);
    if (!user) {
      throw new UnauthorizedException('Incorrect email or password');
    }
    const isPasswordValid = await bcryptjs.compare(loginDto.password, user.password);
    if (!isPasswordValid) {
      throw new UnauthorizedException('Incorrect email or password');
    }
    const payload = { id: user.id, email: user.email, role: user.role };
    const token = await this.jwtService.signAsync(payload);
    return { token, email: user.email, role: user.role };
  }

  async requestPasswordReset(resetPasswordRequestDto: ResetPasswordRequestDto): Promise<{ message: string }> {
    const user = await this.usersService.findOneByEmail(resetPasswordRequestDto.email);
    if (!user) {
      throw new BadRequestException('User does not exist');
    }

    const resetCode = Math.floor(1000 + Math.random() * 9000).toString(); // Generates a 4-digit code
    const hashedResetCode = await bcryptjs.hash(resetCode, 10);
    const expiration = new Date();
    expiration.setHours(expiration.getHours() + 1);
    user.resetToken = hashedResetCode;
    user.resetTokenExpiration = expiration;
    await this.usersService.save(user);

  
    await this.mailerService.sendMail({
      to: 'mauricio.espinosa@alumnos.ucn.cl', 
      subject: 'Reset Your Password',
      text: `Use the following code to reset your password: ${resetCode}`, 
    });

    return { message: 'Reset code sent to your email.' };
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto): Promise<{ message: string }> {
    const user = await this.usersService.findOneBy({
      resetTokenExpiration: MoreThanOrEqual(new Date())
    });
    if (!user || !(await bcryptjs.compare(resetPasswordDto.token, user.resetToken))) {
      throw new UnauthorizedException('Invalid or expired reset code');
    }
    user.password = await bcryptjs.hash(resetPasswordDto.newPassword, 10);
    user.resetToken = null;
    user.resetTokenExpiration = null;
    await this.usersService.save(user);
    return { message: 'Password successfully reset.' };
  }

  async verifyResetToken(token: string): Promise<boolean> {
    const user = await this.usersService.findOneBy({
      resetToken: token,
      resetTokenExpiration: MoreThanOrEqual(new Date())
    });
    return !!user;
  }

  async verifyAuthenticationToken(token: string): Promise<any> {
    try {
      return this.jwtService.verify(token, { secret: jwtConstants.secret });
    } catch (error) {
      console.error('Token verification error:', error);
      throw new UnauthorizedException('Invalid or expired token');
    }
  }
}
