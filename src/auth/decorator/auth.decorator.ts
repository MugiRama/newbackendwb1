import { UseGuards, applyDecorators } from "@nestjs/common";
import { Role } from "../../common/enums/rol.enum";
import { RolesGuard } from "../guard/roles.guard";
import { AuthGuard } from "@nestjs/passport";
import { Roles } from "../roles.decorator";

export function Auth(role: Role){
    return applyDecorators(
        Roles(role), UseGuards(AuthGuard('jwt'), RolesGuard)
    )
}