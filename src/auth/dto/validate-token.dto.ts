// src/auth/dto/validate-token.dto.ts

import { IsString, IsNotEmpty } from 'class-validator';

export class ValidateTokenDto {
    @IsString()
    @IsNotEmpty()
    token: string;
}
