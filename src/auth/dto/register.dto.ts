import { Transform, TransformFnParams } from "class-transformer";
import { IsEmail, IsNotEmpty, IsString, MinLength, IsOptional } from "class-validator";

export class RegisterDto {

    @IsString()
    @MinLength(1)
    name: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @Transform(({ value }: TransformFnParams) => value.trim())
    @IsString()
    @MinLength(6)
    password: string;

    @IsOptional()  
    @IsString()
    rol: string;
}
