import { IsString, MinLength, MaxLength } from 'class-validator';

export class ResetPasswordDto {
    @IsString()
    @MinLength(6)
    newPassword: string;

    @IsString()
    @MaxLength(4)
    @MinLength(4)
    token: string;
}
