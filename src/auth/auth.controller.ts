import { Body, Controller, Query, Get, Post, HttpStatus, HttpException, UseInterceptors, UseGuards, ValidationPipe, UsePipes } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { ResetPasswordRequestDto } from './dto/reset-request-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { ValidateTokenDto } from './dto/validate-token.dto';
import { GeoAuthInterceptor } from 'src/interceptors/geo-auth.interceptor';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Post('register')
    @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
    async register(@Body() registerDto: RegisterDto) {
        try {
            await this.authService.register(registerDto);
            return { message: 'User registered successfully' };
        } catch (error: unknown) {
            const responseError = error as { response?: { data?: { message: string } }, status?: number };
            throw new HttpException(responseError.response?.data?.message || 'Error registering user', responseError.status || HttpStatus.BAD_REQUEST);
        }
    }

    @Post('login')
    @UseInterceptors(GeoAuthInterceptor)
    async login(@Body() loginDto: LoginDto) {
        try {
            return await this.authService.login(loginDto);
        } catch (error: unknown) {
            const message = (error as { message?: string }).message || 'Login failed';
            throw new HttpException(message, HttpStatus.UNAUTHORIZED);
        }
    }

    @Post('request-reset-password')
    async requestResetPassword(@Body() resetPasswordRequestDto: ResetPasswordRequestDto) {
        try {
            await this.authService.requestPasswordReset(resetPasswordRequestDto);
            return { message: 'Check your email for the reset code.' };
        } catch (error: unknown) {
            const message = (error as { message?: string }).message || 'Failed to send reset code';
            throw new HttpException(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Post('reset-password')
    async changePassword(@Body() resetPasswordDto: ResetPasswordDto) {
        try {
            await this.authService.resetPassword(resetPasswordDto);
            return { message: 'Password successfully reset.' };
        } catch (error: unknown) {
            const message = (error as { message?: string }).message || 'Password reset failed';
            throw new HttpException(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Post('validate-token')
    async validateToken(@Body() validateTokenDto: ValidateTokenDto) {
        try {
            const isValid = await this.authService.verifyResetToken(validateTokenDto.token);
            return { isValid };
        } catch (error: unknown) {
            const message = (error as { message?: string }).message || 'Token validation failed';
            throw new HttpException(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Get('validate-reset-token')
    async validateResetToken(@Query('token') token: string): Promise<{ isValid: boolean }> {
        try {
            const isValid = await this.authService.verifyResetToken(token);
            return { isValid };
        } catch (error: unknown) {
            const message = (error as { message?: string }).message || 'Token validation failed';
            throw new HttpException(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Post('validate-auth-token')
    async validateAuthToken(@Body() validateTokenDto: ValidateTokenDto) {
        try {
            const user = await this.authService.verifyAuthenticationToken(validateTokenDto.token);
            if (user) {
                return { isValid: true, user };  
            } else {
                return { isValid: false };
            }
        } catch (error: unknown) {
            const message = (error as { message?: string }).message || 'Token inválido o expirado';
            throw new HttpException(message, HttpStatus.UNAUTHORIZED);
        }
    }
}
